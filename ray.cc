#include <Eigen/Dense>
#include "ray.hh"

using namespace Eigen;

Vector3f Ray::getOrigin() const
{
	return origin;
}

Vector3f Ray::getDirection() const
{
	return direction;
}

void Ray::setOrigin(Vector3f& new_origin)
{
	origin = new_origin;
}

void Ray::setDirection(Vector3f& new_direction)
{
	direction = new_direction;
	updateOnePerComps();
}



void Ray::updateOnePerComps() {

    if(direction(0) != 0.0f) one_per_x_comp = 1.0f / direction(0);
    else one_per_x_comp = 0.0f;

    if(direction(1) != 0.0f) one_per_y_comp = 1.0f / direction(1);
    else one_per_y_comp = 0.0f;

    if(direction(2) != 0.0f) one_per_z_comp = 1.0f / direction(2);
    else one_per_z_comp = 0.0f;

	//one_per_y_comp = 1.0f / direction(1);
	//one_per_z_comp = 1.0f / direction(2);
  
}



float Ray::getPerXComp() const {
	return one_per_x_comp;	
}	
float Ray::getPerYComp() const {
	return one_per_y_comp;	
}	
float Ray::getPerZComp() const {
	return one_per_z_comp;	
}	

