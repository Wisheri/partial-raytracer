#include <iostream>
#include <Eigen/Dense>
#include <Eigen/Geometry>
#include <vector>
#include <omp.h>

#include "scene.hh"
#include "ray.hh"
#include "camera.hh"
#include "light.hh"
#include "shape.hh"
#include "material.hh"
#include "bvhtree.hh"

#define EPSILON 0.0003f
#define MAX_BOUNCES 4

using namespace Eigen;

void Scene::setCamera(Camera* c){
    camera = c;
}

void Scene::addShape(Shape* new_shape)
{
	shapes.push_back(new_shape);
}

void Scene::addLight(Light* new_light)
{
	lights.push_back(new_light);
}

Ray Scene::computeNewRayFromCamera(float x, float y, size_t width, size_t height)
{
	//First calculating the normalized pixel coordinates
	float aspect_ratio = float(height) / float(width);
	float normalized_i = 2.f * (x / width) - 1.f;
	float normalized_j = 2.f * (y / height) - 1.f;

	//Calculating the view planes point for the ray
	float distance = 1.f / tan(camera->getFov() / 2.f);
	Vector3f new_center = camera->getPosition() + distance * camera->getDirection();
	Vector3f new_point = new_center + normalized_i * camera->getRightVector() + normalized_j * camera->getUpVector() * aspect_ratio;

	return Ray(camera->getPosition(), -(new_point - camera->getPosition()).normalized());

}

Ray Scene::computeRefractedRay(const Vector3f& direction, const Vector3f& hit_position, Shape* shape, float rindex, IntersectionType& hit, const Ray& ray)
{
	if (hit != INSIDE) { //Ray is coming to the object from the outside
		float nr = rindex / shape->getMaterial().getRefraction();
		Vector3f normal = shape->getNormal(hit_position);

		float root = 1.f - (nr * nr) * (1.f - ((normal.dot(-direction) * normal.dot(-direction))));

		if (root < 0.f) {	//Total internal reflection
			Vector3f reflection = -direction - 2.f * -direction.dot(normal) * normal;
			return Ray(hit_position + EPSILON * direction, reflection.normalized());
		}

		Vector3f refraction = (nr * normal.dot(-direction) - sqrtf(root)) * normal - (nr * -direction);
		return Ray(hit_position + 0.0009f * direction, refraction.normalized());
	}

	else { //Ray is inside the object
		float nr = shape->getMaterial().getRefraction() / 1.f;
		Vector3f normal = shape->getNormal(hit_position);

		float root = 1.f - (nr * nr) * (1.f - (-normal.dot(-ray.getDirection()) * (-normal.dot(-ray.getDirection()))));
		
		if (root < 0.f) { //Total internal reflection
			Vector3f reflection = ray.getDirection() - 2.f * ray.getDirection().dot(-normal) * -normal;
			return Ray(hit_position - EPSILON * ray.getDirection(), reflection.normalized());
		}

		Vector3f refraction = (nr * (-normal.dot(-ray.getDirection())) - sqrtf(root)) * -normal - ( nr * -ray.getDirection());
		return Ray(hit_position + 0.0009f * ray.getDirection(), refraction.normalized());
	}
}


Ray Scene::computeReflectionRay(const Vector3f& direction, const Vector3f& hit_position, Shape* shape)
{
	//Returns the reflected ray
	Vector3f reflected = direction - 2 * direction.dot(shape->getNormal(hit_position)) * shape->getNormal(hit_position);
	return Ray(hit_position, reflected.normalized());
}

Vector3f Scene::computePhongShading(const Vector3f& normal, const Vector3f& direction_to_light, const Ray& ray, const Vector3f& light_color, Shape* shape, float distance)
{
	//TODO: Implement the affect of the distance from the object to the light!

	Vector3f phong_color = Vector3f(0.f, 0.f, 0.f);
	Vector3f new_reflection = direction_to_light - 2.f * direction_to_light.dot(normal) * normal;
	new_reflection.normalize();

	float tmp = std::max(0.f, std::min(direction_to_light.dot(normal), 1.f));

	phong_color = light_color * (1.f / (distance * distance)) * tmp * shape->getMaterial().getDiffuse();
	
	
	float specular_tmp = std::max(0.f, std::min(ray.getDirection().dot(new_reflection), 1.f));

	if (direction_to_light.dot(normal) >= 0.f) {
		Vector3f specular = light_color * shape->getMaterial().getSpecular();
		for (size_t i = 0; i < shape->getMaterial().getGlosiness(); ++i)
			specular = specular * specular_tmp;
		phong_color += specular;
	}
	
	return phong_color;
	
}

bool Scene::shadowIntersect(const Vector3f& position, const Vector3f& direction, float distance)
{
	//Returns true if the position is in shadow and returns false if it's not
	Ray shadow_ray = Ray(position, direction);
	IntersectionType type;

	float intersect = tree->getShadowHit(shadow_ray, type);
	if (intersect > 0.f && intersect <= distance)
		return true;

	return false;
}


Vector3f Scene::traceRay(const Ray& ray, int bounces, int max_bounces, float rindex)
{
	//Setting the initial values for t_min and min_shape
	float t_min  = -1.f;
	Shape* min_shape = NULL;

	//The initial IntersectionTypes
	IntersectionType type;
	IntersectionType min_type = MISS;

	//If there are no lights in the scene, every point is black
	if (lights.empty())
		return Vector3f(0,0,0);

	//Intersects the objects and finds the closest intersection and stores the intersections distance to t_min and the object to min_shape
	for (auto i : tree->getShapes(ray)) {
		float intersect = shapes[i]->intersect(ray, type);
		if (t_min == -1.f && intersect > 0) {
			t_min = intersect;
			min_shape = shapes[i];
			min_type = type;
		}
		if (intersect > 0 && intersect < t_min) {
			t_min = intersect;
			min_shape = shapes[i];
			min_type = type;
			}
		}


	//Calculate if the ray hits the lights directly
	for (auto light = lights.begin(); light != lights.end(); ++light) {
		float t_light = (*light)->intersect(ray);
		if (t_light > 0 && t_light < t_min) {
			return (*light)->getColor();
		}
		else if (t_min == -1.f && t_light > 0)
			return (*light)->getColor();
	}


	if (min_shape != NULL) { //Intersection happened
		//0.2f == ambient coef
		Vector3f final_color = min_shape->getMaterial().getColor() * 0.2f;
		Vector3f new_position = ray.getOrigin() + (t_min - EPSILON) * ray.getDirection();

		//Calculating reflection
		float reflection = min_shape->getMaterial().getReflection();
		if (reflection > 0.f && bounces < max_bounces) {

			//Computing a new reflection ray and calling this function recursively to calculate the final color of the pixel
			Ray reflectedRay = computeReflectionRay(ray.getDirection(), new_position, min_shape);
			Vector3f reflection_color = traceRay(reflectedRay, bounces + 1, max_bounces, rindex);

			return final_color + reflection_color * reflection; //Returning the object's own color + the new reflected color
		}


		
		//Calculating refraction
		float refraction = min_shape->getMaterial().getRefraction();
		if (refraction > 0.f && bounces < max_bounces) {

			//Computing a new refracted ray and calling this function recursively to calculate the final color of the pixel
			Ray refraction_ray = computeRefractedRay((new_position - ray.getOrigin()).normalized(), new_position, min_shape, rindex, min_type, ray);
			Vector3f refraction_color = traceRay(refraction_ray, bounces + 1, max_bounces, rindex);

			return final_color + refraction_color; //Returning the object's own color + the new color from the refraction
		}


		//Creating a new vector, which stores the directions for the light samples
		//If the light is type point, this will only contain one direction, but if it is an area light, it will contain many samples
		std::vector<Vector3f> light_directions;
		
		//A couple of counters to help determine if the object is in shadow for every point or if some points are in the light
		size_t counter = 0;
		size_t total_count = 0;


		for (auto light = lights.begin(); light != lights.end(); ++light) {
			Vector3f hit_normal = min_shape->getNormal(new_position);

			//Emptying the previous directions
			light_directions.clear();
			(*light)->getDirections(light_directions, new_position); //This function adds the new directions to the vector. The sampling of arealights is done here

			total_count += light_directions.size(); //Adding the number of different samples to the counter

			for (auto i = light_directions.begin(); i != light_directions.end(); ++i) {
				bool intersect = shadowIntersect(new_position, (*i).normalized(), (*i).norm());
				
				if (intersect) //If the object is in shadow
					counter++;
				
				if (hit_normal.dot((*i).normalized()) >= 0 && intersect == false) //Intersection didn't happen
					final_color += computePhongShading(hit_normal, (*i).normalized(), ray, (*light)->getColor(), min_shape, (*i).norm()) / light_directions.size(); //Adding phong shading to the color
			}
		}

		if (counter == total_count) //Every light sample was in a shadow
			final_color = Vector3f(0,0,0);

		return final_color; //Returns the final color of the pixel
	}
	else //Intersection didn't happen, so we can return a black pixel
		return Vector3f(0,0,0);
}


void Scene::render(std::vector<Vector3f>& colors,size_t width, size_t height, size_t sampleN)
{
    size_t i,j;

    //Initialising the bvhtree for use
    //BVHTree tree = BVHTree(shapes);
    if(tree == NULL) {
    	tree = new BVHTree(shapes);
	    //tree = &temp;
    }

    bool antialiasing = false;
    if (sampleN > 0){
        antialiasing = true;
        }

    #pragma omp parallel for collapse(2) //Adding openmp multicore support for the program
	for (j=0; j < height; ++j) {
		for (i=0; i < width; ++i) {

			if (antialiasing) { //If antialiasing is on, calculates multiple samples of every pixel and returns the average from them
				Vector3f color = Vector3f(0,0,0);
				for (size_t sy = 0; sy < sampleN; ++sy) {
					for (size_t sx = 0; sx < sampleN; ++sx) {

						//Divides the pixel into multiple subpixels and sends Rays trough them
						Ray newray = computeNewRayFromCamera(float(i) + (float(sx) / float(sampleN)), float(j) + (float(sy) / float(sampleN)),width,height); 
						color += traceRay(newray, 0, MAX_BOUNCES, 1.0f);
					}
				}
            	colors[j*width+i] = color / float(sampleN * sampleN); //Calculating the average
           	}

           else {
           		//Antialiasing is not on so the program sends only one ray through the middle of the pixel
           		Ray newray = computeNewRayFromCamera(float(i) + 0.5f,float(j) + 0.5f,width,height);
				Vector3f color = traceRay(newray, 0, MAX_BOUNCES, 1.0f);
				colors[j*width+i] = color;
           }
		}
	}


}
