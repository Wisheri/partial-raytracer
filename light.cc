#include <Eigen/Dense>
#include <iostream>
#include "light.hh"
#include "ray.hh"
#include "triangle_shape.hh"
#include "shape.hh"

using namespace Eigen;

Vector3f PointLight::getColor() const
{
	return color;
}

Vector3f PointLight::getPosition() const
{
	return position;
}

void PointLight::setColor(Vector3f& new_color)
{
	color = new_color;
}

Vector3f AreaLight::getColor() const
{
	return color;
}

Vector3f AreaLight::getPosition() const
{
	return position;
}

void AreaLight::setColor(Vector3f& new_color)
{
	color = new_color;
}

float PointLight::intersect(const Ray& ray)
{
	Vector3f light_dir = position - ray.getOrigin();
	if (light_dir.normalized() == -ray.getDirection())
		return light_dir.norm();
	else
		return -1.0f;
}

float AreaLight::intersect(const Ray& ray)
{
	//Creating two triangles and using their intersection fucntions for determining if the ray intersects or not
	Triangle first_triangle = Triangle(Vector3f(1,1,1), position, first_dimension_vector, second_dimension_vector);
	Triangle second_triangle = Triangle(Vector3f(1,1,1), position + (first_dimension_vector - position) + (second_dimension_vector - position), second_dimension_vector, first_dimension_vector);

	IntersectionType type;
	IntersectionType type2;
	float t = first_triangle.intersect(ray, type);
	float t2 = second_triangle.intersect(ray, type2);

	if (t > 0.f)
		return t;
	else if (t2 > 0.f)
		return t2;
	else
		return -1.0f;
}

void PointLight::getDirections(std::vector<Vector3f>& light_directions, const Vector3f& hit_position)
{
	//Returns the direction from the point to the light's position
	Vector3f new_direction = position - hit_position;
	light_directions.push_back(new_direction);
}

void AreaLight::getDirections(std::vector<Vector3f>& light_directions, const Vector3f& hit_position)
{
	//Creating multiple samples to the light
	Vector3f x_vector = (first_dimension_vector - position) / lights_x;
	Vector3f y_vector = (second_dimension_vector - position) / lights_y;

	for (size_t j = 0; j < lights_y; ++j) {
		for (size_t i = 0; i < lights_x; ++i) {

			Vector3f new_direction = position + float(i) * x_vector + float(j) * y_vector - hit_position;
			light_directions.push_back(new_direction);
		}
	}

}