#include <Eigen/Dense>
#include <Eigen/Geometry>
#include "camera.hh"

#define MAX_FOV 2.35f
#define ZOOM_FACTOR 0.8f


using namespace Eigen;

Vector3f Camera::getPosition() const
{
	return position;
}

Vector3f Camera::getDirection() const
{
	return direction;
}

Vector3f Camera::getUpVector() const
{
	return up_vector;
}

Vector3f Camera::getRightVector() const
{
	return right_vector;
}

float Camera::getDistance() const
{
	return distance;
}

float Camera::getFov() const
{
	return fov;
}

void Camera::setDistance()
{
	Vector3f diff = looking_at - position;
	distance = diff.norm();
}

//Calculates cameras direction
void Camera::setDirection()
{
	Vector3f cam_direction = looking_at - position;
	cam_direction.normalize();
	direction = -cam_direction;
}

//Calculates the right vector, which is the cross product of up and direction
void Camera::setRightVector()
{
	Vector3f right = up_vector.cross(direction);
	right_vector = right;
}

void Camera::setUpVector()
{
    up_vector = right_vector.cross(-direction);
}

//Moves the camera according to the command
void Camera::move(Camera::Command& command){
    switch(command) {
        case up: 
        {
            float a = -0.2f;
            Matrix3f rotation;
            rotation << 1,0,0,
                        0, cos(a), -sin(a),
                        0, sin(a), cos(a);

            direction = rotation * direction;
            direction.normalize();
            looking_at = position + direction;
            setUpVector();
            break;
        }
        case down:
        {
            float a = 0.2f;
            Matrix3f rotation;
            rotation << 1,0,0,
                        0, cos(a), -sin(a),
                        0, sin(a), cos(a);

            direction = rotation * direction;
            direction.normalize();
            looking_at = position + direction;
            setUpVector();
            break;
        }
        case right:
        {
            float a = 0.2f;
            Matrix3f rotation;
            rotation << cos(a), 0, sin(a),
                0,1,0,
                -sin(a), 0, cos(a);

            direction = rotation * direction;
            direction.normalize();
            looking_at = position + direction;
            setRightVector();
            break;
        }
        case left:
        {
            float a = -0.2f;
            Matrix3f rotation;
            rotation << cos(a), 0, sin(a),
                0,1,0,
                -sin(a), 0, cos(a);

            direction = rotation * direction;
            direction.normalize();
            looking_at = position + direction;
            setRightVector();
            break;
        }
        case u:
        {   
            position = position + move_distance * up_vector;
            looking_at = position + move_distance * up_vector;
            break;
        }
        case d:
        { 
            position = position - move_distance * up_vector;
            looking_at = position - move_distance * up_vector;
            break;
        }
        case r:
        { 
            position = position - move_distance * right_vector;
            looking_at = position - move_distance * right_vector;
            break;
        }
        case l:
        {  
            position = position + move_distance * right_vector;
            looking_at = position + move_distance * right_vector;
            break;
        }
        case f:
        {   
            position = position - move_distance * direction;
            looking_at = position - move_distance * direction;
            break;
        }
        case b:
        {   
            position = position + move_distance * direction;
            looking_at = position + move_distance * direction;
            break;
        }
        case z: //zoom in
            fov=ZOOM_FACTOR*fov;
            break;
        case x: //zoom out
            if(fov/ZOOM_FACTOR<MAX_FOV){ //if smaller than 135 degrees            
                fov=fov/ZOOM_FACTOR;
            }
            else fov = MAX_FOV;
            break;
        default:
            break;
    }
}
