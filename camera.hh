#ifndef RAYTRACER_CAMERA_HH
#define RAYTRACER_CAMERA_HH

#include <Eigen/Dense>
#include <Eigen/Geometry>

using namespace Eigen;

class Camera
{
public:

    enum Command {up, down, right, left, u, d, r, l, f, b, z, x};

	Camera(Vector3f pos=Vector3f::Zero(), Vector3f look=Vector3f::Zero(), Vector3f up=Vector3f::Zero(), float field_of_view = 90, float move_dist = 0.1f) : position(pos), looking_at(look), up_vector(up), move_distance(move_dist), fov(field_of_view){
		setDistance();
		setDirection();
		setRightVector();
	}

    //RO3
	~Camera() {}
    Camera(const Camera& other) = delete;
    Camera& operator=(const Camera& other) = delete;

	//Returns camera's position
	Vector3f getPosition() const;

	//Returns the direction vector
	Vector3f getDirection() const;

	//Returns the vector pointing up (camera's orientation)
	Vector3f getUpVector() const;

	//Returns the vector pointing right (camera's orientation)
	Vector3f getRightVector() const;

    //Returns the field of view in angles
    float getFov() const;

	//Returns distance
	float getDistance() const;

	//Calculates distance
	void setDistance();

	//Calculates cameras direction
	void setDirection();

	//Calculates the vector pointing right
	void setRightVector();

	//Calculates the vector pointing up
	void setUpVector();

    //Moves the camera according to the command
    void move(Camera::Command& command);

private:
	Vector3f position; //Cameras position
	Vector3f looking_at; //A point where camera is looking at
	Vector3f direction; //Noramlized direction vector
	Vector3f up_vector; //Normalized up vector
	Vector3f right_vector; //Normalized right vector
	float distance; //Distance between the camera and the looking_at vector, which defines the viewing plane
	float move_distance; // Common distance for move-commands
	float fov; //Field of view
};


#endif
