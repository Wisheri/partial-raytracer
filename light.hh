#ifndef RAYTRACER_LIGHT_HH
#define RAYTRACER_LIGHT_HH

#include <Eigen/Dense>
#include "ray.hh"
#include "shape.hh"
#include "triangle_shape.hh"

using namespace Eigen;

class Light
{
public:
	Light() { }

	virtual ~Light() {};

	//Returns light's color
	virtual Vector3f getColor() const = 0;

	//Returns light's position
	virtual Vector3f getPosition() const = 0;

	//Changes lights color
	virtual void setColor(Vector3f& new_color) = 0;

	//Calculates if the ray intersects with the light
	virtual float intersect(const Ray& ray) = 0;

	//Calculates the light samples for calculating shadows
	virtual void getDirections(std::vector<Vector3f>& light_directions, const Vector3f& hit_position) = 0;

	Light(const Light&) = delete;
	Light& operator=(const Light&) = delete;

};


class PointLight : public Light
{
public:
	PointLight(Vector3f pos, Vector3f col = Vector3f(1,1,1)) : position(pos), color(col) {}

	Vector3f getColor() const;

	Vector3f getPosition() const;

	float intersect(const Ray& ray);

	void setColor(Vector3f& new_color);

	void getDirections(std::vector<Vector3f>& light_directions, const Vector3f& hit_position);

private:
	Vector3f position;
	Vector3f color;
};

class AreaLight : public Light
{
public:
	AreaLight(Vector3f pos, Vector3f fdm, Vector3f sdm, size_t x, size_t y, Vector3f col = Vector3f(1,1,1)) : position(pos), first_dimension_vector(fdm), second_dimension_vector(sdm), lights_x(x), lights_y(y), color(col) {
}

	Vector3f getColor() const;

	Vector3f getPosition() const;

	float intersect(const Ray& ray);

	void setColor(Vector3f& new_color);

	void getDirections(std::vector<Vector3f>& light_directions, const Vector3f& hit_position);


private:
	Vector3f position;
	Vector3f first_dimension_vector;
	Vector3f second_dimension_vector;
	size_t lights_x;
	size_t lights_y;
	Vector3f color;
};


#endif
