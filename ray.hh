#ifndef RAYTRACER_RAY_HH
#define RAYTRACER_RAY_HH

#include <Eigen/Dense>

using namespace Eigen;

class Ray
{
public:
	Ray(Vector3f ori, Vector3f dir) : origin(ori), direction(dir) { updateOnePerComps(); }
	Ray() : origin(0,0,0), direction(0,0,0) { updateOnePerComps(); }
	
	//Returns ray's origin
	Vector3f getOrigin() const;
	
	//Returns ray's direction
	Vector3f getDirection() const;

	//Set a new origin for the ray
	void setOrigin(Vector3f& new_origin);

	//Set a new direction for the ray
	void setDirection(Vector3f& new_direction);
	

	////to make BVHTree faster:
	void updateOnePerComps();
	float getPerXComp() const; 
	float getPerYComp() const;
	float getPerZComp() const;
	////


private:
	Vector3f origin;
	Vector3f direction;
	
	////to make BVHTree faster:
	float one_per_x_comp;	// = 1 / direction(0)
	float one_per_y_comp;	// = 1 / direction(1)
	float one_per_z_comp;	// = 1 / direction(2)
	////
};

#endif
