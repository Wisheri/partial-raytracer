#ifndef RAYTRACER_SCENE_HH
#define RAYTRACER_SCENE_HH
#define _USE_MATH_DEFINES

#include <Eigen/Dense>
#include "ray.hh"
#include "camera.hh"
#include "light.hh"
#include "shape.hh"
#include "bvhtree.hh"

using namespace Eigen;

class Scene
{
public:
	Scene(std::vector<Shape*> sha, std::vector<Light*> li, Camera* cam) : shapes(sha), lights(li), camera(cam) {
	    tree = new BVHTree(shapes);
	} //this constructor is for testing and debugging
	Scene() : camera(NULL), tree(NULL) {} //constructor used by actual GUI-based program

    //RO3
	~Scene() {
	    if (tree) delete tree;
        if (camera) delete camera;
        while(!lights.empty()) delete lights.back(), lights.pop_back();
        while(!shapes.empty()) delete shapes.back(), shapes.pop_back();
    }
    Scene(const Scene& oher) = delete;
    Scene& operator=(const Scene& other) = delete;

    //Set camera (if camera is already set, replaces the older camera)
    void setCamera(Camera* c);

	//Add a new shape to the scene
	void addShape(Shape* new_shape);

	//Add a new light to the scene
	void addLight(Light* new_light);

	//Computes a new ray from the camera given the 2D pixel coordinates x and y
	Ray computeNewRayFromCamera(float x, float y, size_t width, size_t height);

	//Computes a refracted ray
	Ray computeRefractedRay(const Vector3f& direction, const Vector3f& hit_position, Shape* shape, float rindex, IntersectionType& hit, const Ray& ray);

	//Computes a new ray for reflection
	Ray computeReflectionRay(const Vector3f& direction, const Vector3f& hit_position, Shape* shape);

	//Computes the Phong shading
	Vector3f computePhongShading(const Vector3f& normal, const Vector3f& direction_to_light, const Ray& ray, const Vector3f& light_color, Shape* shape, float distance);

	//Calculates if the shadow ray intersects with an object or not
	bool shadowIntersect(const Vector3f& position, const Vector3f& direction, float distance);

	//Ray tracing
	Vector3f traceRay(const Ray &ray, int bounces, int max_bounces, float rindex);

	//Renders the image
	void render(std::vector<Vector3f>& colors, size_t width, size_t height, size_t sampleN = 0);

    //Accessors
    Camera* getCamera() { return camera; }

private:
	std::vector<Shape*> shapes;
	std::vector<Light*> lights; 
	Camera* camera;
	BVHTree* tree;
};


#endif
